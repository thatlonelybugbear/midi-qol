import { debugEnabled, i18n, warn } from "../../midi-qol.js";
import { Workflow } from "../Workflow.js";
import { ReplaceDefaultActivities, configSettings } from "../settings.js";
import { asyncHooksCall } from "../utils.js";
import { MidiActivityMixin, MidiActivityMixinSheet } from "./MidiActivityMixin.js";

export var MidiSummonActivity;
export var MidiSummonSheet;

export function setupSummonActivity() {
  if (debugEnabled > 0) warn("MidiQOL | SummonActivity | setupSummonActivity | Called");
  //@ts-expect-error
  const GameSystemConfig = game.system.config;
  //@ts-expect-error
  MidiSummonSheet = defineMidiSummonSheetClass(game.system.applications.activity.SummonSheet);
  MidiSummonActivity = defineMidiSummonActivityClass(GameSystemConfig.activityTypes.summon.documentClass);
  if (ReplaceDefaultActivities) {
    // GameSystemConfig.activityTypes["dnd5eSummon"] = GameSystemConfig.activityTypes.summon;
    GameSystemConfig.activityTypes.summon = { documentClass: MidiSummonActivity };
  } else {
    GameSystemConfig.activityTypes["midiSummon"] = { documentClass: MidiSummonActivity };
  }
}
let defineMidiSummonSheetClass = (baseClass: any) => {
  return class MidiSummonSheet extends MidiActivityMixinSheet(baseClass) {
  }
}

let defineMidiSummonActivityClass = (ActivityClass: any) => {
  return class MidiSummonActivity extends MidiActivityMixin(ActivityClass) {
    static LOCALIZATION_PREFIXES = [...super.LOCALIZATION_PREFIXES, "midi-qol.SUMMON"];
    static metadata =
      foundry.utils.mergeObject(
        super.metadata, {
        title: configSettings.activityNamePrefix ? "midi-qol.SUMMON.Title.one" : ActivityClass.metadata.title,
        dnd5eTitle: ActivityClass.metadata.title,
        sheetClass: MidiSummonSheet,
        usage: {
          chatCard: "modules/midi-qol/templates/activity-card.hbs",
          dialog: ActivityClass.metadata.usage.dialog,
          actions: {
            // placeSummons: MidiSummonActivity.#placeSummons
          },
        },
      }, { inplace: false, insertKeys: true, insertValues: true });

    static #placeSummons(event, target, message) {
      //@ts-expect-error
      if (message) this.workflow = Workflow.getWorkflow(message.uuid);
     ActivityClass.#placeSummons.bind(this)(event, target, message);
    }
    get possibleOtherActivity() {
      return false;
    }
    get isTriggerableActivity() {
      return true;
    }

    get selfTriggerableOnly() {
      return false;
    }
  }
}
